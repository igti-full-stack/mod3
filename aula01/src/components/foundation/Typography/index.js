const Typography = ({ children, component }) => {

  if(!!component && component.toLowerCase() === 'h3') {
    return <h3>{children}</h3>
  }

  return (
    <p>
      {children}
    </p>
  )
}

export default Typography
