import React from 'react'

function Toggle({ description, enabled, onToggle }) {
  return (
    <div className="switch">
      <label>
        {description}
        <input type="checkbox" onChange={onToggle} checked={enabled} />
        <span className="lever"></span>
      </label>
    </div>
  )
}

export default Toggle
