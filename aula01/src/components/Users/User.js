import Avatar from '../dataDisplay/Avatar'
import css from './user.module.css'

export default function User({ user }) {
  const divUser = user

  return (
    <div className={css.flexRow}>
      <Avatar
        src={divUser.picture.large}
        alt={divUser.name.first}
      />
      <span>{divUser.name.first}</span>
    </div>
  )
}
