const Avatar = ({ src, alt }) => {

  return (
    <div>
      <img style={styles.avatar} src={src} alt={alt} />
    </div>
  )
}

const styles = {
  avatar: {
    borderRadius: '50%',
    marginRight: '5px',
    width: '50px',
    height: '50px',
  }
}

export default Avatar


