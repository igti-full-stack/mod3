import React, { useEffect, useState } from 'react'

import Users from './components/Users'
import Header from './infra/components/Header'

import './App.css'

function App() {
  const [users, setUsers] = useState([])
  const [showUsers, setShowUsers] = useState(false)

  useEffect(() => {
    const fetchUsers = async () => {
      const res = await fetch('https://randomuser.me/api/?seed=rush&nat=br&results=11')
      const json = await res.json()
      setUsers(json.results)
    }
    fetchUsers()
  }, [])

  const handleShowUsers = (isChecked) => {
    console.log('toggle: ', isChecked.target.checked)
    setShowUsers(isChecked.target.checked)
  }

  return (
    <div className="App">
      <Header
        title={'React LifeCycle'}
        showUsers={showUsers}
        handleShowUsers={handleShowUsers}
      />
      {showUsers && <Users users={users} />}
    </div>
  )
}

export default App
