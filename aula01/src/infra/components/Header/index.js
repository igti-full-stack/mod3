import Typography from "../../../components/foundation/Typography"
import Toggle from "../../../components/Toggle"

const Header = ({title, showUsers, handleShowUsers}) => {
  return (
    <div>      
      <Typography component='h3'>
        {title}
      </Typography>
      <Toggle
        description="Mostrar usuários:"
        enabled={showUsers}
        onToggle={handleShowUsers} 
      />
      <hr />
    </div>
  )
}

export default Header
