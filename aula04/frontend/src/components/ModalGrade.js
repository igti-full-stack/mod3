import { useEffect, useState } from 'react'
import Modal from 'react-modal'

import * as api from '../api/apiService'

Modal.setAppElement('#root')

export default function ModalGrade({ onSave, onClose, selectedGrade }) {

  const [gradeValue, setGradevalue] = useState(selectedGrade.value)
  const [gradeValidation, setGradeValidation] = useState({})
  const [errorMessage, setErrorMessage] = useState('')
  const { id, student, subject, type } = selectedGrade

  useEffect(() => {
    const getValidation = async () => {
      const validation = await api.getValidationGradeType(type)
      setGradeValidation(validation)
    }
    getValidation()
  }, [type])

  useEffect(() => {
    const { minValue, maxValue } = gradeValidation

    if (gradeValue < minValue || gradeValue > maxValue) {
      setErrorMessage(`O valor da nota deve ser entre ${minValue} e ${maxValue}`)
      return
    }
    setErrorMessage('')
  }, [gradeValue, gradeValidation])

  useEffect(() => {    
    document.addEventListener('keydown', handleKeyDown)
    return () => {
      document.removeEventListener('keydown', handleKeyDown)
    }
  })

  const handleKeyDown = (event) => {
    // o computador não está ouvindo a tecla ESC
    if (event.key.toLowerCase() === 'q') {
      onClose(null)
    }
  }

  const handleFormSubmit = (event) => {
    event.preventDefault()    
    const formData = {
      id, 
      newValue: gradeValue,
    }
    onSave(formData)
  }

  const handleGradeChange = (event) => {
    setGradevalue(+event.target.value)
  }

  const handleModalClose = () => {
    onClose(null)
  }

  return (
    <div>
      <Modal isOpen={true}>

        <div style={styles.flexRow}>
          <span style={styles.title}>Manutenção de notas</span>
          <button className="waves-effect waves-light btn red dark-4" onClick={handleModalClose}>
            x
          </button>
        </div>

        <form onSubmit={handleFormSubmit}>
          <div className="input-field">
            <input type="text" id="inputName" value={student} readOnly />
            <label className="active" htmlFor="inputName">
              Nome do aluno:
            </label>
          </div>

          <div className="input-field">
            <input type="text" id="inputSubject" value={subject} readOnly />
            <label className="active" htmlFor="inputSubject">
              Disciplina:
            </label>
          </div>

          <div className="input-field">
            <input type="text" id="inputType" value={type} readOnly />
            <label className="active" htmlFor="inputType">
              Tipo de avaliação:
            </label>
          </div>

          <div className="input-field">
            <input
              type="number"
              id="inputGrade"
              min={gradeValidation.minValue}
              max={gradeValidation.maxValue}
              step='1'
              autoFocus
              value={gradeValue}
              onChange={handleGradeChange}
            />
            <label htmlFor="inputGrade" className="active">Nota: </label>
          </div>

          <div style={styles.flexRow}>
            <button
              className="waves-effect waves-light btn"
              disabled={errorMessage.trim() !== ''}
            >
              Salvar
            </button>
            <span style={styles.errorMessage}>{errorMessage}</span>
          </div>
        </form>
      </Modal>
    </div>
  )
}

const styles = {
  flexRow: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginBottom: '40px',
  },
  title: {
    fontSize: '1.3rem',
    fontWeight: 'bold',
  },
  flexStart: {
    justifyContent: 'flex-start'
  },
  errorMessage: {
    color: 'red',
    fontWeight: 'bold',
  }
}
