import { useEffect, useState } from 'react';

import './App.css';
import Countries from './components/Countries';
import Typography from './components/foundation/Typography';
import Header from './infra/components/Header';

function App() {
  const [allCountries, setAllCountries] = useState([])
  const [filter, setFilter] = useState('')
  const [filteredCountries, setFilteredCountries] = useState([])
  const [filteredPopulation, setFilteredPopulation] = useState(0)

  useEffect(() => {
    const fetchCountries = async () => {
      const res = await fetch('http://restcountries.eu/rest/v2/all')
      const json = await res.json()

      const allCountriesJson = json.map(({ name, numericCode, flag, population, translations }) => {
        return {
          id: numericCode,
          name,
          name_pt: translations.pt,
          filterName: translations.pt.toLowerCase(),
          flag,
          population
        }
      })

      setAllCountries(allCountriesJson)
      setFilteredCountries([...allCountriesJson])

      const population = calculatePopulation(allCountriesJson)
      setFilteredPopulation(population)
    }
    fetchCountries()
  }, [])

  const handleChangeFilter = (newText) => {
    setFilter(newText)
    const filterLowerCase = newText.toLowerCase()
    const filtered = allCountries.filter(country => {
      return country.filterName.includes(filterLowerCase)
    })
    setFilteredCountries(filtered)

    const totalPopulation = calculatePopulation(filtered)
    setFilteredPopulation(totalPopulation)
  }

  const calculatePopulation = (countries) => {

    const totalPopulation = countries.reduce((accumulator, current) => {
      return accumulator + current.population
    }, 0)

    return totalPopulation
  }

  return (
    <div className="container">
      <Typography size='heading'>
        React Coutries
      </Typography>
      <Header
        filter={filter}
        onChangeFilter={handleChangeFilter}
        countryCount={filteredCountries.length}
        totalPopulation={filteredPopulation}
      />
      <Countries countries={filteredCountries} />
    </div>
  );
}

export default App;
