import Typography from "../../../components/foundation/Typography"
import { formatNumber } from "../../../helpers/formatHelpers"

import css from './header.module.css'

export default function Header({ filter, onChangeFilter, countryCount, totalPopulation }) {

  const handleInputChange = (event) => {
    const newText = event.target.value
    onChangeFilter(newText)
  }

  return (
    <div className={css.flexRow}>
      <input
        placeholder="Filtro"
        type="text"
        value={filter}
        onChange={handleInputChange}
        autoFocus={true}
      /> |{' '}
      <Typography className={css.countries}>
        Países: <strong>{countryCount}</strong>
      </Typography> |{' '}
      <Typography className={css.population}>
        População: <strong>{formatNumber(totalPopulation)}</strong>
      </Typography>
    </div>

  )
}