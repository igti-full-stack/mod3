import Country from "./Country"
import css from './countries.module.css'

export default function Countries(props) {

  return (
    <div className={`${css.border} ${css.flexRow}`}>
        {
          props.countries.map(country => (            
              <Country key={country.id} country={country} />
          ))
        }
    </div>
  )
}