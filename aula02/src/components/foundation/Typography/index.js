import css from './typography.module.css'

const Typography = ({ children, size, className }) => {
  if (!!size && size.toLowerCase() === 'heading') {
    return <h2 style={styles.centeredTitle}>{children}</h2>
  }

  return (
    <span className={className}>{children}</span>
  )
}

const styles = {
  centeredTitle: {
    textAlign: 'center',
  },
}

export default Typography

