import css from './avatar.module.css'

const Avatar = ({ src, alt }) => {
  return (
    <img src={src} alt={alt} />
  )
}

export default Avatar
