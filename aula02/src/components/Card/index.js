import Avatar from "../Avatar"
import CardBody from "./CardBody"

import css from './card.module.css'

const Card = ({ avatar, pais }) => {
  return (
    <div className={`${css.card} ${css.border}`}>
      <Avatar src={avatar.src} alt={avatar.alt} />
      <CardBody description={pais} />
    </div>
  )
}

export default Card
