import Typography from "../foundation/Typography"

import css from './card.module.css'

const CardBody = ({ description }) => {
  return (
    <Typography className={css.cardText}>
      {description}
    </Typography>
  )
}

export default CardBody
