import CountUp from 'react-countup'

export default function Votes({ value, previous }) {
  return (
    <div>
      <CountUp
        start={previous}
        end={value}
        duration={.8}
        separator="."       
      >
        {({ countUpRef }) => (
          <div>
            <span ref={countUpRef} />            
          </div>
        )}
      </CountUp>
    </div>
  )
}