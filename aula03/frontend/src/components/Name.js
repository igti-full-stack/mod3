export default function Name({ children }) {
  return (
    <div style={{fontWeight: 'bold'}}>
      {children}
    </div>
  )
}