import Info from "./Info"
import Name from "./Name"
import Percentage from "./Percentage"
import Picture from "./Picture"
import Popularity from "./Popurarity"
import Position from "./Position"
import Votes from "./Votes"
import { formatNumber, formatPercentage } from '../helpers/formatHelpers'

import css from './candidate.module.css'

export default function Cadidate({ candidate, position, previousVote, previousPercentage }) {
  const { id, name, votes, percentage, popularity } = candidate
  const imageSource = `${id}.jpg`

  return (
    <div className={css.flexRow}>
      <Position>{position}</Position>
      <Picture imageSource={imageSource} description={name} />
      <Info>
        <Name>{name}</Name>
        <Votes value={votes} previous={previousVote} />
        <Percentage previous={previousPercentage} value={percentage} />
        <Popularity value={popularity} />
      </Info>

    </div>
  )
}
